package racionales;

import java.util.Scanner;

/**
 * Clase encargada de representar a los numeros racionales.
 * @author diego
 */
public class Racionales {
    
    // Variable que representa al numerador.
    int p;
    // Variable que representa al denominador.
    int q;
    
    /*
     * Constructor por default (Sin parametros).
    */
    Racionales(){
        this.p = 1;
        this.q = 1;
    }
    
    /*
     * Constructor por parametros.
    */
    Racionales(int p, int q){
        this.p = p;
        this.q = q;
    }
    
    /**
     * Metodo que asigna un nuevo valor al numerador.
     * @param p El nuevo valor del numerador.
     */
    private void setNumerador(int p){
        this.p = p;
    }
    
    /**
     * Metodo que asigna un nuevo valor al denominador.
     * @param q El nuevo valor del denominador.
     */
    private void setDenominador(int q){
        this.q = q;
    }
    
    /**
     * Metodo que regresa el valor del numerador.
     * @return El valor del numerador.
     */
    private int getNumerador(){
        return this.p;
    }
    
    /**
     * Metodo que regresa el valor del denominador.
     * @return El valor del denominador.
     */
    private int getDenominador(){
        return this.q;
    }
    
    /**
     * Metodo que imprime el numero racional.
     */
    void show(){
        if(this.getNumerador() == 0)
            System.out.println("0");
        else if(this.getNumerador() == this.getDenominador())
            System.out.println("1");
        else
            System.out.print(getNumerador() + "/" + getDenominador());
    }
    
    /**
     * A este metodo no se le puede cambiar el nombre como en el caso de show.
     * Override es una etiqueta que indica que el metodo se esta sobre escribiendo
     * @return Regresa una cadena que representa al objeto.
     */
    @Override public String toString(){
        return this.getNumerador() + "/" + this.getDenominador();
    }
    
    /**
     * Metodo que realiza la multiplicacion de dos numeros racionales.
     * @param r El numero racional con el que multiplicaremos.
     * @return El resultado de la multiplicacion.
     */
    Racionales multiplicaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        p = getNumerador() * r.getNumerador();
        q = getDenominador() * r.getDenominador();
        
        // Regresamos el racional simplificado.
        return (new Racionales(p, q)).simplifica();
    }
    
    /**
     * Metodo que realiza la division de dos numeros racionales.
     * @param r El numero racional con el que dividiremos.
     * @return El resultado de la division.
     */
    Racionales divideRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        p = getNumerador() * r.getDenominador();
        q = getDenominador() * r.getNumerador();
        
        // Regresamos el racional simplificado.
        return (new Racionales(p, q)).simplifica();
    }
    
    /**
     * Metodo que realiza la suma de dos numeros racionales.
     * @param r El numero racional con el que sumaremos.
     * @return El resultado de la suma.
     */
    Racionales sumaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        q = mcm(getDenominador(), r.getDenominador());
        
        // this hace referencia al objeto que esta llamando a sumaRacionales,
        // podemos omitirlo como hicimos en los metodos de multiplicacion y division.
        p = (q * this.getNumerador() / this.getDenominador()) + (q * r.getNumerador() / r.getDenominador());
        
        // Regresamos el racional simplificado.
        return (new Racionales(p, q)).simplifica();
    }
    
    /**
     * Metodo que realiza la resta de dos numeros racionales.
     * @param r El numero racional con el que sumaremos.
     * @return El resultado de la suma.
     */
    Racionales restaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        q = mcm(getDenominador(), r.getDenominador());
        
        // this hace referencia al objeto que esta llamando a sumaRacionales,
        // podemos omitirlo como hicimos en los metodos de multiplicacion y division.
        p = (q * this.getNumerador() / this.getDenominador()) - (q * r.getNumerador() / r.getDenominador());
        
        // Regresamos el racional simplificado.
        return (new Racionales(p, q)).simplifica();
    }
    
    /**
     * Metodo encargado de simplificar un numero racional.
     * @return El numero racional simplificado.
     */
    Racionales simplifica(){
        int p, q, mcd;
        
        // Si el numerador es 0, ent. regresamos el mismo numero racional.
        if(this.getNumerador() == 0)
            return (this);
        
        mcd = mcd(this.getNumerador(), this.getDenominador());
        
        p = this.getNumerador() / mcd;
        q = this.getDenominador() / mcd;
        
        return (new Racionales(p, q));
    }

    /**
     * Metodo encargado de calcular el Maximo comun divisor.
     * Uso el algoritmo de Euclides.
     * @param n1 El primer numero.
     * @param n2 El segundo numero.
     * @return El maximo comun divisor de n1 y n2.
     */
    private int mcd(int n1, int n2){
        int mcd = 0;
        int a = 0;
        int b = 0;
        
        // a sera el mayor de los dos numeros.
        /*
        if(n1 > n2)
            a = n1;
        else
            a = n2;
        */
        a = Math.max(n1, n2);
        
        // b sera el menor de los dos numeros.
        /*
        if(n1 < n2)
            b = n1;
        else
            b = n2;
        */
        b = Math.min(n1, n2);
        
        // Esto es lo mismo que for(;b != 0;)
        while(b != 0){
            mcd = b;
            b = a % b;
            a = mcd;
        }
        
        return mcd;
    }
    
    /**
     * Metodo encargado de calcular el Minimo comun multiplo.
     * Usando el maximo comun divisor.
     * @param n1 El primer numero.
     * @param n2 El segundo numero.
     * @return El minimo comun multiplo de n1 y n2.
     */
    private int mcm(int n1, int n2){
        int mcm = 0;
        int a = 0;
        int b = 0;
        
        // a sera el mayor de los dos numeros.
        /*
        if(n1 > n2)
            a = n1;
        else
            a = n2;
        */
        a = Math.max(n1, n2);
        
        // b sera el menor de los dos numeros.
        /*
        if(n1 < n2)
            b = n1;
        else
            b = n2;
        */
        b = Math.min(n1, n2);
        
        mcm = (a / mcd(a, b)) * b;
        
        return mcm;
    }
    
    /**
     * Metodo encargado de calcular la raiz cuadrada de un numero racional.
     * @return El resultado de la raiz cuadrada.
     */
    Racionales raizCuadradaRacionales(){
        int p = 0;
        int q = 1;
        
        p = (int)Math.sqrt(this.getNumerador());
        q = (int)Math.sqrt(this.getDenominador());
        
        return (new Racionales(p, q)).simplifica();
    }
    
    /**
     * Metodo encargado calcular la potencia de un numero racional.
     * @param exp El exponente.
     * @return El resultado.
     */
    Racionales potenciaRacionales(int exp){
        int p =  0;
        int q = 1;
        
        p = (int)Math.pow(this.getNumerador(), exp);
        q = (int)Math.pow(this.getDenominador(), exp);
        
        return (new Racionales(p, q)).simplifica();
    }
    
    /**
     * Metodo que compara dos numeros racionales.
     * @param r El numero racional con el que se comparara.
     */
    void comparaRacionales(Racionales r){
        int p = 0;
        int q = 1;
        
        int a, b;
        
        a = this.getDenominador() * r.getNumerador();
        b = r.getDenominador() * this.getNumerador();
        
        if(a < b){
            //this.show();
            System.out.print(this + " es mayor que " + r);
            //r.show();
        }
        else if(a > b){
            //this.show();
            System.out.print(this + " es menor que " + r);
            //r.show();
        }
        else {
            //this.show();
            System.out.print(this + " es igual a " + r);
            //r.show();
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Creamos dos objetos que representaran a dos numeros racionales.
        Racionales r1 = new Racionales(1, 15);
        Racionales r2 = new Racionales(3, 30);
        
        // Declaramos un objeto (OJO!!!, aun no creamos el objeto).
        Racionales r3;
        
        // Imprimimos r1:
        // Con toString podemos imprimir el objeto sin tener que llamar al metodo.
        System.out.println("R1 es: " + r1);
        //r1.show();
        
        // Imprimimos r2:
        System.out.println("R2 es: " + r2);
        //r2.show();
        
        // En r3 guardaremos los resultados.
        // MULTIPLICACION:
        r3 = r1.multiplicaRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.println("El resultado de la multiplicacion es: " + r3);
        //r3.show();
        
        // DIVISION:
        r3 = r1.divideRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.println("El resultado de la division es: " + r3);
        //r3.show();
        
        // SUMA:
        r3 = r1.sumaRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.println("El resultado de la suma es: " + r3);
        //r3.show();
        
        // RESTA:
        r3 = r1.restaRacionales(r2);
        
        // Imprimimos el resultado.
        System.out.println("El resultado de la resta es: " + r3);
        //r3.show();
        
        // RAIZ CUADRADA:
        r3 = r1.raizCuadradaRacionales();
        
        // Imprimimos el resultado.
        System.out.println("El resultado de la raiz cuadrada es: " + r3);
        //r3.show();
        
        // ELEVAMOS AL CUADRADO:
        r3 = r2.potenciaRacionales(2);
        
        // Imprimimos el resultado.
        System.out.println("El resultado de elevar al cuadrado a r2 es: " + r3);
        //r3.show();
        
        // ELEVAMOS AL CUBO:
        r3 = r1.potenciaRacionales(3);
        
        // Imprimimos el resultado.
        System.out.println("El resultado de elevar al cubo a r1 es: " + r3);
        //r3.show();
        
        // COMPARAMOS R1 Y R2:
        r1.comparaRacionales(r2);
        
        Scanner s = new Scanner(System.in);
        
        int op = 0, p1, q1, p2, q2;
        
        while(op != 9){
            System.out.println("\n¿Que quieres hacer?\n 1) Sumar\n 2) Restar\n "
                    + "3) Multiplicar\n 4) Dividir\n 5) Simplificar\n 6) Elevar al...\n "
                    + "7) Sacar raiz\n 8) Comparar\n 9) Salir\n");
            
            op = s.nextInt();
        
            switch(op){
                case 1:
                    System.out.println("Da el primer numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el primer denominador:");
                    q1 = s.nextInt();
                    System.out.println("Da el segundo numerador:");
                    p2 = s.nextInt();
                    System.out.println("Da el segundo denominador:");
                    q2 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r2 = new Racionales(p2, q2);
                    r3 = r1.sumaRacionales(r2);

                    r1.show();
                    System.out.print(" + ");
                    r2.show();
                    System.out.print(" = ");
                    r3.show();
                    break;
                case 2:
                    System.out.println("Da el primer numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el primer denominador:");
                    q1 = s.nextInt();
                    System.out.println("Da el segundo numerador:");
                    p2 = s.nextInt();
                    System.out.println("Da el segundo denominador:");
                    q2 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r2 = new Racionales(p2, q2);
                    r3 = r1.restaRacionales(r2);

                    r1.show();
                    System.out.print(" - ");
                    r2.show();
                    System.out.print(" = ");
                    r3.show();
                    break;
                case 3:
                    System.out.println("Da el primer numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el primer denominador:");
                    q1 = s.nextInt();
                    System.out.println("Da el segundo numerador:");
                    p2 = s.nextInt();
                    System.out.println("Da el segundo denominador:");
                    q2 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r2 = new Racionales(p2, q2);
                    r3 = r1.multiplicaRacionales(r2);

                    r1.show();
                    System.out.print(" * ");
                    r2.show();
                    System.out.print(" = ");
                    r3.show();
                    break;
                case 4:
                    System.out.println("Da el primer numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el primer denominador:");
                    q1 = s.nextInt();
                    System.out.println("Da el segundo numerador:");
                    p2 = s.nextInt();
                    System.out.println("Da el segundo denominador:");
                    q2 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r2 = new Racionales(p2, q2);
                    r3 = r1.divideRacionales(r2);

                    r1.show();
                    System.out.print(" / ");
                    r2.show();
                    System.out.print(" = ");
                    r3.show();
                    break;
                case 5:
                    System.out.println("Da el numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el denominador:");
                    q1 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r3 = r1.simplifica();

                    r1.show();
                    System.out.print(" => ");
                    r3.show();
                    break;
                case 6:
                    System.out.println("Da el numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el denominador:");
                    q1 = s.nextInt();
                    System.out.println("Da el exponente:");
                    int exp = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r3 = r1.potenciaRacionales(exp);

                    System.out.print("(");
                    r1.show();
                    System.out.print(")^" + exp + " = ");
                    r3.show();
                    break;
                case 7:
                    System.out.println("Da el numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el denominador:");
                    q1 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r3 = r1.raizCuadradaRacionales();

                    System.out.print("raiz(");
                    r1.show();
                    System.out.print(") = ");
                    r3.show();
                    break;
                case 8:
                    System.out.println("Da el primer numerador:");
                    p1 = s.nextInt();
                    System.out.println("Da el primer denominador:");
                    q1 = s.nextInt();
                    System.out.println("Da el segundo numerador:");
                    p2 = s.nextInt();
                    System.out.println("Da el segundo denominador:");
                    q2 = s.nextInt();

                    r1 = new Racionales(p1, q1);
                    r2 = new Racionales(p2, q2);
                    r1.comparaRacionales(r2);
                    
                    break;
                case 9:
                    System.out.println("Bye.");
                    break;
                default:
                    System.out.println("Error: opcion no disponible.");
                    break;
            }
        }
    }
    
}
